#!/usr/bin/env python3
'''
 ----------------------------------------------------------------------------
 "THE BEER-WARE LICENSE" (Revision 42):
  As long as you retain this notice you can do whatever you want with this stuff. 
  If we meet some day, and you think this stuff is worth it, 
  you can buy me a beer in return.   Justin Benge
 ----------------------------------------------------------------------------

 @author: Justin Benge <justinbng36@gmail.com>
'''
import base64
import os
import sys, subprocess
import argparse
from cryptography.fernet import Fernet
from pathlib import Path
from getpass import getpass
import pyperclip
import time
import platform
import sqlite3

#Local imports
import my_database as db
from print_formatting import bcolors, ascii_codes

from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

#adding acomment for git

VERSION = '5.0.1'

DIR_SLASH='/'
WINDOWS=platform.system() == 'Windows'
if(WINDOWS):
    DIR_SLASH = '\\'

PASS_STORE_LOCATION=str(Path.home()) + DIR_SLASH + '.passwords' + DIR_SLASH
SALT_FILE=str(Path.home()) + DIR_SLASH + '.salt'
DB_FILE = str(Path.home()) + DIR_SLASH + '.passwords' + DIR_SLASH + r"pythonsqlite.db"


def version():
 '''Prints the version number, and license info '''
 print('''
   /\/\\    
  / /  \\    /\\        pypass v'''+VERSION+'''
 / / /\ \\  /  \\  /\\   ''', end = '')
 print('''
 \ \/ / /  \\  /  \\/   
  \  / /    \\/    
   \/\/               This program may be freely redistributed under
		      the terms of the Beerware licence.
    ''')

def print_help():
    '''
    Prints how to use the pypass script
    '''
    print("""usage: pypass.py [-h] [-v] [--initialize] [-i INSERT] [-g GET] [--generate GENERATE] [-n LENGTH] [-c COPY]

optional arguments:
  -h, --help            show this help message and exit
  -v, --version
  --initialize
  -i INSERT, --insert INSERT
  -g GET, --get GET
  --generate GENERATE
  -n LENGTH, --length LENGTH
  -c COPY, --copy COPY""")

def print_pass_store(dirs_param=PASS_STORE_LOCATION, padding=0):
    '''
    Prints the names of the passwords
    '''
    conn = db.get_connection(DB_FILE)
    password_list =  db.get_all_data(conn)

    print(bcolors.HEADER + 'Password Store:' + bcolors.ENDC)

    for password in password_list:
        print(ascii_codes.ELBOW + ascii_codes.BAR + bcolors.WARNING + password + bcolors.ENDC)
        #print(password)

def get_key():
    '''
    Generates a key based off of a string or 'password'
    '''
    password = getpass("Master password: ").encode()
    salt_file = Path(SALT_FILE)
    if(salt_file.is_file()):
        with open(salt_file, "rb") as salt_fil:
            salt = salt_fil.read()
    else:
        salt = os.urandom(16)
        with open(salt_file, 'wb') as salt_fil:
            salt_fil.write(salt)

    kdf = PBKDF2HMAC(
         algorithm=hashes.SHA256(),
         length=32,
         salt=salt,
         iterations=100000,
         backend=default_backend()
    )
    key = base64.urlsafe_b64encode(kdf.derive(password))
    return key

def initialize_pass_store():
    '''
    Checks to see if a key is already stored somewhere,
    and creates a new one if one doesn't exist
    '''
    pass_dir = Path(PASS_STORE_LOCATION)
    if(not pass_dir.is_dir()):
        os.mkdir(PASS_STORE_LOCATION)
        if(WINDOWS):
            subprocess.run(["attrib", "+s", "+h", pass_dir])
        conn = db.get_connection(DB_FILE)
        db.create_table(conn)
        db.close_connection(conn)
        print('Created password store :)')

def copy_to_clipboard(key, pass_name):
    '''
        Copies the password to the clipboard

        @param key:
            The key to unlock the password
        @param pass_name:
            The password in question
    '''
    text = pyperclip.paste()
    pyperclip.copy(get_password(key, pass_name))
    print("{0} copied to clipboard for 30 seconds".format(pass_name))
    time.sleep(30)
    pyperclip.copy(text)

def store_password(key, pass_name, password=None):
    '''
    Stores the password

    :param key:
        The key to lock the password
    :param pass_name:
        The name of the password
    :param password:
        This is used if the script is passwing a password from generate_pass...
    '''
    conn = db.get_connection(DB_FILE)
    tmp_password = None
    yn = ''
    try:
        password_temp = db.get_data(conn, pass_name)
        yn = input("Password already exists for {0}. Do you want to overwrite it? [y/N]: ".format(pass_name))
        if yn.lower() == 'n':
            sys.exit(1)
    except IndexError:
        pass
    while(password == None):
        if(tmp_password != None):
            print('Passwords do not match try again')
        password = getpass('Enter password for {0}: '.format(pass_name))
        tmp_password = getpass('Enter password again: ')
        if(password != tmp_password):
            password = None
        
    f = Fernet(key)
    password = password.encode()
    
    db.insert_data(conn, pass_name, f.encrypt(password), yn=='y')
    db.close_connection(conn)

def get_password(key, pass_name):
    '''
    Get the password from the password store

    :param key:
        The key to unlock the password
    :param pass_name:
        The name of the password to get
    :return:
        The password, in plain text...
    '''
    conn = db.get_connection(DB_FILE)
    
    try:
        password = db.get_data(conn, pass_name)
        db.close_connection(conn)
        f = Fernet(key)
        return f.decrypt(password).decode("utf-8")

    except IndexError:
        print("Password for {0} does not exist :(".format(pass_name))
        sys.exit(1)

    except Exception as e:
        print("Whoops somethign went wrong")
        print(e)
        sys.exit(1)
      
def generate_password(key, pass_name, pass_len=20):
    '''
    Generates a random password, idk if it's cryptographically secure (?)

    @param: key:
        The key to unlock the password
    @param pass_name:
        The name of the password file to unlock
    @param pass_len:
        The length of the password to generate (defaulted to 20)
    '''
    password = os.urandom(pass_len)
    password = base64.b64encode(password).decode('utf-8')
    store_password(key,pass_name, password[0:pass_len]) 
    return get_password(key, pass_name) 


parser = argparse.ArgumentParser()
parser.add_argument('-v','--version', action='count'   ) 
parser.add_argument('--initialize'  , action='count'   )
parser.add_argument('-i', '--insert', nargs=1, type=str)
parser.add_argument('-g', '--get'   , nargs=1, type=str)
parser.add_argument('--generate'    , nargs=1, type=str)
parser.add_argument('-n', '--length', nargs=1, type=int)
parser.add_argument('-c', '--copy'  , nargs=1, type=str)
args = parser.parse_args(sys.argv[1:])

if args.version:
    version()
elif args.initialize:
    initialize_pass_store()
elif args.insert:
    store_password(get_key(), args.insert[0])
elif args.get:
    print(get_password(get_key(), args.get[0]))
elif args.copy:
    copy_to_clipboard(get_key(), args.copy[0])
elif args.generate:
    if args.length:
        generate_password(get_key(), args.generate[0], args.length[0])
    else:
        print(generate_password(get_key(), args.generate[0]))
elif not args.generate and args.length:
    print("Please specify --generate")
else:
    print_pass_store()
