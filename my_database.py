import sqlite3

from sqlite3 import Error

TABLE_NAME = 'passwords'
TABLE_SQL = """ CREATE TABLE IF NOT EXISTS """ + TABLE_NAME + """(
                    id integer PRIMARY KEY,
                    name text NOT NULL,
                    password text NOT NULL
                );"""

def get_connection(db_file):
    '''
    The function to connect
    '''
    conn = None
    try:
        #whoa passing in :memory: to the connect function creates a db in memory
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

def create_table(conn):
    '''
    This unction to create table
    '''
    try:
        c = conn.cursor()
        c.execute(TABLE_SQL)
    except Error as e:
        print(e)

def insert_data(conn, name, password, update=False):
    '''
    '''
    cursor = conn.cursor()

    if(not update):
        sql = 'INSERT INTO ' + TABLE_NAME + ' (name, password) VALUES (?,?)'
        cursor.execute(sql, (name,password))
    else:
        sql = 'UPDATE ' + TABLE_NAME + ' set password=? WHERE name=?'
        cursor.execute(sql, (password,name))
    conn.commit()
        
def get_data(conn, name=""):
    '''
    '''
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM " + TABLE_NAME + " WHERE name=?",(name,))
    
    row = cursor.fetchall()[0]

    return row[2]

def delete_data(conn, name=''):
    cursor = conn.cursor()
    try:
        cursor.execute('DELETE FROM ' + TABLE_NAME + ' WHERE name=?',(name,))
        conn.commit()
        return True
    except Error as e:
        return False,e

def close_connection(conn):
    conn.close()
    return True

def get_all_data(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM " + TABLE_NAME)
    
    rows = cursor.fetchall()
    retval = []
    for row in rows:
        retval.append(row[1])

    return retval


'''
if __name__ == '__main__':
    conn = create_connection(str(Path.home()) + DIR_SLASH + '.passwords' + DIR_SLASH + r"pythonsqlite.db")
    create_table(conn)
    insert_data(conn, 'gitlab.com/jbenge1', 'gAAAAABgWlaMvjn4Qtmy1FUufe5GKOhvsHURvinSEVux4T175V69UgLacophuw67dwmVdeRV0ix0uWv7cVJEmrCMlIeA19flPw==')
    get_data(conn,"gitlab.com/jbenge1")
    close_connection(conn)
'''
