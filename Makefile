.PHONY: build install

build: pypass.c
	pyinstaller pypass.c --onefile

install:
	@install -m755 dist/pypass /usr/local/bin

clean:
	rm -r __pycache__ build dist pypass.spec

text:
	echo Hello World