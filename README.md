# PyPass
_I reccommend reading through the installation process and usage once completely before installing_

**Please report all bugs via gitlab issues to keep them in a central location for me**

A password manager implemented using python
# Table of Contents
1. [Installation](#installation)
2. [Development](#development)
3. [Prerequisites](#prerequisites)
4. [Usage](#usage)
5. [Purpose](#purpose)
6. [Notes](#notes)
7. [Other Considerations](#other-considerations)

## Installation

### All platforms:
- clone the repo: `git clone https://gitlab.com/jbenge1/pypass`
- `cd pypass`
- At some point in the future this should all be taken care of by the makefile. But that future is not here yet
- **Note** You can use either `pip3` or `conda` (whichever you prefer)
- For `pip`:
    - `pip3 install pyinstaller cryptography argparse pypyerclip`
    - `pyinstaller pypass.py --onefile`
- For `conda`:
    - `conda install pyinstaller cryptography argparse getpass`
    - `conda install -c conda-forge pyperclip`
- See [note](#notes) at bottom for `/path/to/your/homemade/scripts/` for both *nix and MacOs
### \*nix:
- `install -Dm755 dist/pypass /path/to/your/homemade/scripts/`
### MacOs:
- `install -Cm755 dist/pypass /path/to/your/homemade/scripts/`
### Windows (sigh....):
- _yeah... so this was designed specifically as a tool for windows, and I did no research on installing on windows_:grimacing:.
- Here is what **I THINK** (please correct me if there is a better way [what I read](https://docs.python.org/3/faq/windows.html)
    - add `.py` to PATHEXT [environment vairable](https://www.nextofwindows.com/what-is-pathext-environment-variable-in-windows)
    - `copy dist\pypass.exe C:\Users\%username%\AppData\Local\Programs\Python\Python3?\Scripts`
        - - You may also need to add `C:\Users\%username%\AppData\Local\Programs\Python\Python3?\Scripts` to your `PATH` Variable
    - If there is a better way to do this, or a better spot to keep it, I'd like to know!
    - For colored output use the following reghack: in `HKEY_CURRENT_USER\Console` create a `DWORD` named `VirtualTerminalLevel` and set it to `0x1`

### All platforms (again):
- run `pypass -v` for a sanity check and some fun art
- at this point pypass is installed. But to use it other than seeing some help text (`pypass -h`) or more of the same art (probably boring by now) you need to initialize the pass store. (See below)

## Development

### In progress:
- Adding a gui
- Migrating passwords to new versions? (I think this isn't an issue now that we can confidently recreate keys)
- Changing the structure of the password store to website/username so multiple passwowrds could be stored under 1 domain

### In the pipeline:
- Cooler ascii art

## Prerequisites

This project utilizes several python builtins as well as some non standard (?) python tools:
- **[Cryptography](https://pypi.org/project/cryptography/)** A tool to generate keys and encrypt passwords
- **[Argparse](https://docs.python.org/3/library/argparse.html)** A module to help the writer make user-friendly command line interfaces.
- **[getpass](https://docs.python.org/2/library/getpass.html)** prompt the user for a password without echoing it to cli (getpass I think is now included as a standard module?)
- **[pyinstaller](http://www.pyinstaller.org/)** create an executable python file
- **[pyperclip](https://github.com/asweigart/pyperclip)** copy things to windows clipboard
- Thats it... :shrug:

## Usage

- For the time being this is merely a cli tool so you will need to use either windows' command prompt, or a bash emulator (I use termite on my linux box and the builtin terminal (whatever it's called) for macos)
- if this is a fresh install run `pypass --initialize`. This will just create the password store
- to create a new password entry run `pypass -i name_of_website/username` EX: `pypass -i stackoverflow.com/kewlDude2020`. It will prompt you to enter a 'master' password. **This password is not stored anywhere so remember it** This is the password used to lock and unlock your passwords
    - It will ask you for another password this is the password to store for the stie
    - The name of the website will be stored as a dir/folder so you can store multiple 'things' (need a better word) under a single site Eg: You can have a store like `archlinux.org/\[forums\]\[aur\]\[wiki\]etc...` 
    - you can also nest them something like `tracker.ceph.com/projects/ceph-deploy/issuses/username` where everything but _username_ is a dir/folder
- to get a password you have already inserted, run `pypass -g name_of_website/username`. EX: `pypass -g stackoverflow.com/kewlDude2020` It will prompt you for a password again, this is your master password to unlock the password entry. It will print the password out to the screen if the master password is matched.
    - If you don't want your password printed to `stdout` then run `pypass -c name_of_website-username` EX: `pypass -c stackoverflow.com-kewlDude2020` it will again prompt you for the master password, but instead will copy the password to the clipboard for 30 seconds.
- to generate passswords run `pypass -g name_of_website/username -n N` EX: `pypass -g name_of_website/username -n 15` which will generate a random password of length N, store the password as if `pypass -i ...` was typed and print the password out to the screen for inspection.
- run `pypass -v` for a cool ascii picture and what version you're running (probably only cool the first time)
- run `pypass -h` for a not so helpful printout :grimacing:
- As of version 3.0.0 all folders in windows are hidden, including the password store, so you will not be able to navigate to them via the file explorer unless you know the exact path

## Purpose

I don't think (and I'm sure i'm not the only one) passwords should be stored online using such tools as
googles new password manager, (not to say that they aren't far more secure than this 'toy'). There are
aleready several password managers, such as [pass](https://www.passwordstore.org/) there don't seem to be
many for windows that [store your passwords locally](https://is.gd/aebXbY) (or I'm just really bad using the internet :persevere:).
While i do use pass,it would seem the project for [windows](https://github.com/mbos/Pass4Win#readme) has been 
abandoned :disappointed:. Thus the introduciton of **PyPass** :smile:


## Notes

If you don't already have something setup such as a folder to store your homemade scripts then this is the place you want to be. 
    
I'll outline my highly opinionated guide on how to do this. If you think it should be done differently then:

1. You don't need to be in this section
1. Don't follow my instructions (or do, I'm not telling you what to do)

**Instructions**
1. Create a folder in your home directory: `mkdir ~/.bin` 
1. Update your path variable: `echo export PATH="$PATH:~/.bin" >> ~/.bashrc` or (zsh)(fish)(dish)(icantkeeptracksh)
1. `source ~/.bashrc`
1. From here on, anywhere you see `/path/to/your/homemade/scripts/` replace it with `~/.bin`

## Other Considerations

:shrug:
